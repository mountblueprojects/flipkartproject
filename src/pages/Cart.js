import React, { Component } from "react";
import MainCart from "../components/MainCart";
import Signup from "../components/Signup";
import "./Cart.css";

export class Cart extends Component {
  render() {
    return (
      <div className="cartPage">
        <Signup
          state={this.props.state}
          onSubmit={this.props.onSubmit}
          onClick={this.props.onClick}
        />
        <MainCart state={this.props.state} cartClick={this.props.cartClick} />
      </div>
    );
  }
}

export default Cart;
