import React, { Component } from "react";
import SingleProduct from "../components/SingleProduct";
import Signup from "../components/Signup";

export class Product extends Component {
  render() {
    return (
      <>
        <SingleProduct
          id={this.props.match.params.id.split(":")[1]}
          addToCart={this.props.addToCart}
        />
        <Signup
          state={this.props.state}
          onSubmit={this.props.onSubmit}
          onClick={this.props.onClick}
        />
      </>
    );
  }
}

export default Product;
