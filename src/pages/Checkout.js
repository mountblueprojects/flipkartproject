import React, { Component } from "react";
import "./Checkout.css";
import PriceDetails from "../components/PriceDetails";
import Signup from "../components/Signup";
import EmptyCart from "../components/EmptyCart";

export class Checkout extends Component {
  render() {
    return (
      <div className="checkoutPageContainer">
        {Object.keys(this.props.state.cart).length === 0 && (
          <EmptyCart state={this.props.state} />
        )}
        {Object.keys(this.props.state.cart).length > 0 && (
          <div className="checkoutPage">
            <Signup
              state={this.props.state}
              onSubmit={this.props.onSubmit}
              onClick={this.props.onClick}
            />
            <div className="checkoutDetails">
              <div className="accordion detailsAccordion" id="accordionExample">
                <div className="accordion-item ">
                  <h2 className="accordion-header" id="headingOne">
                    <button
                      className={
                        this.props.state.signupSuccess
                          ? "accordion-button btn-primary btn collapsed"
                          : "accordion-button btn-primary btn"
                      }
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded={
                        this.props.state.signupSuccess ? "false" : "true"
                      }
                      aria-controls="collapseOne"
                    >
                      Login
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    className={
                      this.props.state.signupSuccess
                        ? "accordion-collapse collapse"
                        : "accordion-collapse collapse show"
                    }
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    {!this.props.state.userLogged && (
                      <div className="accordion-body text-center">
                        <button
                          id="checkoutLogin"
                          className="btn btn-warning fs-5 "
                          onClick={this.props.onClick}
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModal"
                        >
                          Login to your Account
                        </button>
                      </div>
                    )}

                    {this.props.state.userLogged && (
                      <div className="accordion-body text-center">
                        <p className="fs-5">Already Logged In!</p>
                      </div>
                    )}
                  </div>
                </div>
                <div className="accordion-item ">
                  <h2 className="accordion-header" id="headingTwo">
                    <button
                      className={
                        this.props.state.signupSuccess &&
                        !this.props.state.hasAddress
                          ? "accordion-button btn-primary btn"
                          : "accordion-button collapsed btn-primary btn"
                      }
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo"
                      aria-expanded={
                        this.props.state.signupSuccess &&
                        !this.props.state.hasAddress
                          ? "true"
                          : "false"
                      }
                      aria-controls="collapseTwo"
                      disabled={!this.props.state.signupSuccess}
                    >
                      Address
                    </button>
                  </h2>

                  <div
                    id="collapseTwo"
                    className={
                      this.props.state.signupSuccess &&
                      !this.props.state.hasAddress
                        ? "accordion-collapse collapse show"
                        : "accordion-collapse collapse"
                    }
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      {this.props.state.hasAddress && (
                        <div>
                          <div className="card addressDetailsCard">
                            <div className="card-body savedAddress p-3 ">
                              <h5 className="card-title">
                                {this.props.state.name}
                              </h5>
                              <h6 className="card-subtitle mb-2 text-muted">
                                {this.props.state.mobile}
                              </h6>
                              <p className="card-text fs-6 mb-0">
                                {this.props.state.locality}
                              </p>
                              <p className="card-text fs-6 mb-0">
                                {this.props.state.addressArea}
                              </p>
                              <p className="card-text fs-6 mb-0">
                                {this.props.state.city}
                              </p>
                              <p className="card-text fs-6 mb-0">
                                {this.props.state.state}
                              </p>
                            </div>
                          </div>
                          <div className="editDelete">
                            <button
                              className="btn btn-warning editAddress mt-3"
                              onClick={this.props.addressSubmit}
                            >
                              Edit
                            </button>
                            <button
                              className="btn btn-danger deleteAddress mt-3"
                              onClick={this.props.addressSubmit}
                            >
                              <i class="fa-solid fa-trash"></i>
                            </button>
                          </div>
                        </div>
                      )}
                      {!this.props.state.hasAddress && (
                        <form
                          className="row g-3"
                          id="addressForm"
                          onSubmit={this.props.addressSubmit}
                        >
                          <div className="row mb-3 mt-3">
                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="text"
                                placeholder="Name"
                                aria-label="Name"
                                defaultValue={this.props.state.name}
                              />
                              {this.props.state.nameError && (
                                <p className="addressError">
                                  Enter a valid name
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="number"
                                placeholder="10-digit mobile number"
                                aria-label="10-digit mobile number"
                                defaultValue={this.props.state.mobile}
                              />
                              {this.props.state.mobileError && (
                                <p className="addressError">
                                  Enter a valid phone number
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="number"
                                placeholder="Pincode"
                                defaultValue={this.props.state.pincode}
                              />
                              {this.props.state.pincodeError && (
                                <p className="addressError">
                                  Enter a valid pincode
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="text"
                                placeholder="Locality"
                                defaultValue={this.props.state.locality}
                              />
                              {this.props.state.localityError && (
                                <p className="addressError">
                                  Enter a valid locality
                                </p>
                              )}
                            </div>

                            <div className="col-12">
                              <textarea
                                className="form-control mb-3"
                                placeholder="Address(Area and Street)"
                                defaultValue={this.props.state.addressArea}
                              />
                              {this.props.state.addressError && (
                                <p className="addressError">
                                  Enter a valid address
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                placeholder="City/District/Town"
                                type="text"
                                defaultValue={this.props.state.city}
                              />
                              {this.props.state.cityError && (
                                <p className="addressError">
                                  Enter a valid city
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <select className="form-select mb-3">
                                <option selected>
                                  {this.props.state.state}
                                </option>
                                <option>Andaman and Nicobar Islands</option>
                                <option>Andhra Pradesh</option>
                                <option>Arunachal Pradesh</option>
                                <option>Assam</option>
                                <option>Bihar</option>
                                <option>Chandigarh</option>
                                <option>Chhattisgarh</option>
                                <option>Dadra and Nagar Haveli</option>
                                <option>Daman and Diu</option>
                                <option>Goa</option>
                                <option>Gujarat</option>
                                <option>Haryana</option>
                                <option>Himachal Pradesh</option>
                                <option>Jammu and Kashmir</option>
                                <option>Jharkhand</option>
                                <option>Karnataka</option>
                                <option>Kerala</option>
                                <option>Lakshadweep</option>
                                <option>Madhya Pradesh</option>
                                <option>Maharashtra</option>
                                <option>Manipur</option>
                                <option>Meghalaya</option>
                                <option>Mizoram</option>
                                <option>Nagaland</option>
                                <option>
                                  National Capital Territory of Delhi
                                </option>
                                <option>Odisha</option>
                                <option>Pondicherry</option>
                                <option>Punjab</option>
                                <option>Rajasthan</option>
                                <option>Sikkim</option>
                                <option>Tamil Nadu</option>
                                <option>Telangana</option>
                                <option>Tripura</option>
                                <option>Uttar Pradesh</option>
                                <option>Uttarakhand</option>
                                <option>West Bengal</option>
                              </select>
                              {this.props.state.stateError && (
                                <p className="addressError">
                                  Please pick a valid state
                                </p>
                              )}
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="text"
                                placeholder="Landmark(Optional)"
                                defaultValue={this.props.state.landmark}
                              />
                            </div>

                            <div className="col-md-6">
                              <input
                                className="form-control mb-3"
                                type="number"
                                placeholder="Alternate Phone(Optional)"
                                defaultValue={this.props.state.alternate}
                              />
                            </div>

                            <p>
                              <small className="text-muted">Address Type</small>
                            </p>

                            <div id="radioButtons" className="mb-3">
                              <div>
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name="delivery"
                                  id="home"
                                  defaultChecked
                                />
                                <label className="form-check-label" for="home">
                                  Home{"(All day Delivery)"}
                                </label>
                              </div>
                              <div>
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name="delivery"
                                  id="work"
                                />
                                <label className="form-check-label" for="work">
                                  Work{"(Delivery between 10AM - 5PM)"}
                                </label>
                              </div>
                            </div>

                            <div id="saveButtons" className="mt-3">
                              <button
                                className="btn btn-warning"
                                id="save"
                                type="submit"
                              >
                                SAVE AND DELIVER HERE
                              </button>
                            </div>
                          </div>
                        </form>
                      )}
                    </div>
                  </div>
                </div>

                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className={
                        this.props.state.hasAddress
                          ? "accordion-button btn-primary btn"
                          : "accordion-button collapsed btn-primary btn"
                      }
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseThree"
                      aria-expanded={
                        this.props.state.hasAddress ? "true" : "false"
                      }
                      aria-controls="collapseThree"
                      disabled={!this.props.state.hasAddress}
                    >
                      Payment Methods
                    </button>
                  </h2>

                  <div
                    id="collapseThree"
                    className={
                      this.props.state.hasAddress
                        ? "accordion-collapse collapse show"
                        : "accordion-collapse collapse"
                    }
                    aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <div className="row mb-3 mt-3">
                        <div className="col-12">
                          <input
                            className="form-check-input mb-3"
                            name="payment"
                            id="upi"
                            type="radio"
                          />
                          <label className="form-check-label" for="upi">
                            UPI
                          </label>
                        </div>

                        <div className="col-12">
                          <input
                            className="form-check-input mb-3"
                            name="payment"
                            id="wallet"
                            type="radio"
                          />
                          <label className="form-check-label" for="wallet">
                            Wallets
                          </label>
                        </div>

                        <div className="col-12">
                          <input
                            className="form-check-input mb-3"
                            name="payment"
                            id="credit"
                            type="radio"
                          />
                          <label className="form-check-label" for="credit">
                            Credit / Debit Card
                          </label>
                        </div>

                        <div className="col-12">
                          <input
                            className="form-check-input mb-3"
                            name="payment"
                            id="emi"
                            type="radio"
                          />
                          <label className="form-check-label" for="emi">
                            EMI
                          </label>
                        </div>

                        <div className="col-12">
                          <input
                            className="form-check-input mb-3"
                            name="payment"
                            id="cod"
                            type="radio"
                          />
                          <label className="form-check-label" for="cod">
                            Cash On Delivery
                          </label>
                        </div>

                        <div className="col-md-12 proceed">
                          <button id="proceed" className="btn btn-warning">
                            PROCEED TO PAYMENT
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <PriceDetails state={this.props.state} />
          </div>
        )}
      </div>
    );
  }
}

export default Checkout;
