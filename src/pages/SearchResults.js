import React, { Component } from "react";
import MainSearch from "../components/MainSearch";
import Signup from "../components/Signup";

export class SearchResults extends Component {
  render() {
    return (
      <div>
        <Signup
          state={this.props.state}
          onSubmit={this.props.onSubmit}
          onClick={this.props.onClick}
        />
        <MainSearch state={this.props.state} />
      </div>
    );
  }
}

export default SearchResults;
