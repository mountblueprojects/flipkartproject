import React, { Component } from "react";
import products from "../products.json";
import "./Category.css";
import UpperSection from "../components/UpperSection";
import { Link } from "react-router-dom";

export class Category extends Component {
  render() {
    return (
      <div className="categoryProducts">
        <UpperSection
          onSubmit={this.props.onSubmit}
          state={this.props.state}
          onClick={this.props.onClick}
          categoryClick={this.props.categoryClick}
        />

        {products
          .filter((product) => {
            return product.category === this.props.match.params.category;
          })
          .map((product) => {
            return (
              <Link to={`/product:${product.id}`}>
                <div className="card categorySearch p-3">
                  <div className="row g-0 categoryResultContainer">
                    <div className="col-md-12 col-lg-3 categorySearchImage">
                      <img
                        src={product.image}
                        className="img-fluid rounded-start categorySearchPicture"
                        alt="phone"
                      />
                    </div>

                    <div className="col-md-12 col-lg-4 categoryMainBody">
                      <div className="card-body categoryMainDetails">
                        <h5 className="card-title">{product.title}</h5>
                        <div className="categorySearchRating">
                          {product.rating.rate}
                          <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg==" />
                        </div>

                        <ul>
                          {product.description.map((detail) => {
                            return <li>{detail}</li>;
                          })}
                        </ul>
                      </div>
                    </div>

                    <div className="col-md-12 col-lg-5 categorySideBody">
                      <div className="card-body categoryPrice">
                        <h4 className="card-title">{product.price}</h4>
                        <img
                          src="https://static-assets-web.flixcart.com/fk-p-linchpin-web/fk-cp-zion/img/fa_62673a.png"
                          alt="assured"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </Link>
            );
          })}
      </div>
    );
  }
}

export default Category;
