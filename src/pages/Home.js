import React, { Component } from "react";
import Header from "../components/Header";
import Article from "../components/Article";
import Footer from "../components/Footer";

export class Home extends Component {
  render() {
    return (
      <Article
        onSubmit={this.props.onSubmit}
        state={this.props.state}
        onClick={this.props.onClick}
        categoryClick={this.props.categoryClick}
      />
    );
  }
}

export default Home;
