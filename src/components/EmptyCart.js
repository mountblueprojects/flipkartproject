import React, { Component } from "react";
import "./EmptyCart.css";
import { Link } from "react-router-dom";

export class EmptyCart extends Component {
  render() {
    return (
      Object.keys(this.props.state.cart).length === 0 && (
        <div className="emptyCartPage">
          <div className="card text-center emptyCartContainer">
            <div className="card-body emptyCartCard">
              <img
                src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90"
                alt="emptyCart"
              />
              <div className="fs-5">Your cart is empty!</div>
              <div className="fs-6">Add items to it now</div>
              <Link to="/" className="btn btn-primary">
                Shop Now
              </Link>
            </div>
          </div>
        </div>
      )
    );
  }
}

export default EmptyCart;
