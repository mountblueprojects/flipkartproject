import React, { Component } from "react";
import { Link } from "react-router-dom";
import products from "../products.json";
import "./SingleProduct.css";

export class SingleProduct extends Component {
  render() {
    return products
      .filter((product) => {
        return product.id === parseInt(this.props.id);
      })
      .map((product) => {
        return (
          <div
            className="singleProduct d-flex"
            style={{ minHeight: "78.5vh", alignItems: "center" }}
            key={product.id}
          >
            <div
              className="card singleProductCard flex-grow-1 flex-shrink-1 ms-3 justify-content-between"
              style={{ height: "50vh" }}
            >
              <img src={product.image} className="card-img-top" alt="product" />
              <div className="card-body singleProductButton">
                <button
                  className={`btn btn-warning ${product.id} `}
                  onClick={this.props.addToCart}
                >
                  <i className="fa-solid fa-cart-shopping"></i> ADD TO CART
                </button>
              </div>
            </div>

            <div className="singleProductDetails flex-grow-1 flex-shrink-1">
              <h6 className="card-subtitle mb-2 text-muted">
                <Link to="/">Home</Link> {" > "}
                <Link to={`/category/${product.category}`}>
                  {product.category}
                </Link>
                {" > "}
                <Link to={`/product:${product.id}`}>
                  {product.title.substring(0, 25)}...
                </Link>
              </h6>
              <h4>{product.title}</h4>
              <div className="singleProductRating">
                {product.rating.rate}
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                  alt="star"
                />
              </div>
              <h3>{product.price}</h3>
              <ul>
                {product.description.map((detail, index) => {
                  return <li key={`${product.id}-${index}`}>{detail}</li>;
                })}
              </ul>
            </div>
          </div>
        );
      });
  }
}

export default SingleProduct;
