import React, { Component } from "react";
import "./MainSearch.css";
import products from "../products.json";
import { Link } from "react-router-dom";

export class MainSearch extends Component {
  render() {
    return (
      <div className="searchContainer">
        {!this.props.state.search && (
          <div className="card text-center enterSearch">
            <p>Search for products, brands and more</p>
            <img
              className="img-fluid"
              src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90"
              alt="enterSearch"
            />
          </div>
        )}

        {this.props.state.search && (
          <div className="showResults">
            <p className="fs-5 fw-semibold">
              Showing results for "{this.props.state.search}"
            </p>
          </div>
        )}

        {this.props.state.search &&
          !products.filter((product) => {
            return product.title
              .toLowerCase()
              .includes(this.props.state.search.toLowerCase());
          }).length &&
          !products.filter((product) => {
            return (
              product.category
                .toLowerCase()
                .includes(this.props.state.search.toLowerCase()) ||
              product.type
                .toLowerCase()
                .includes(this.props.state.search.toLowerCase())
            );
          }).length &&
          !products.filter((product) => {
            return product.description.filter((detail) => {
              return detail
                .toLowerCase()
                .includes(this.props.state.search.toLowerCase);
            }).length;
          }).length && (
            <div className="noResults">
              <div className="card text-center border-light noResultsCard">
                <img
                  src="https://static-assets-web.flixcart.com/fk-p-linchpin-web/fk-cp-zion/img/error-no-search-results_2353c5.png"
                  className="card-img-top"
                  alt="NoResults"
                />
                <div className="card-body">
                  <p className="card-title fs-4 fw-bold">
                    Sorry, no results found!
                  </p>
                  <p className="card-subtitle fs-5 text-muted">
                    Please check the spelling or try searching for something
                    else
                  </p>
                </div>
              </div>
            </div>
          )}

        {this.props.state.search &&
          products
            .filter((product) => {
              return product.title
                .toLowerCase()
                .includes(this.props.state.search.toLowerCase());
            })

            .map((product) => {
              return (
                <Link className="mainSearch" to={`/product:${product.id}`}>
                  <div className="card result">
                    <div className="row g-0 resultContainer">
                      <div className="col-md-6 searchImage">
                        <img
                          src={product.image}
                          className="img-fluid rounded-start searchPicture"
                          alt="product"
                        />
                      </div>

                      <div className="col-md-6">
                        <div className="card-body mainBody p-3">
                          <h5 className="card-title">{product.title}</h5>
                          <div className="searchRating">
                            {product.rating.rate}
                            <img
                              className="img-fluid"
                              src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                            />
                          </div>

                          <p className="card-text">
                            <ul>
                              {product.description.slice(0, 3).map((detail) => {
                                return <li>{detail}</li>;
                              })}
                            </ul>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              );
            })}
        {this.props.state.search &&
          products
            .filter((product) => {
              return (
                product.category
                  .toLowerCase()
                  .includes(this.props.state.search.toLowerCase()) ||
                product.type
                  .toLowerCase()
                  .includes(this.props.state.search.toLowerCase())
              );
            })

            .map((product) => {
              return (
                <Link to={`/product:${product.id}`} className="mainSearch">
                  <div className="card  result">
                    <div className="row g-0 resultContainer">
                      <div className="col-md-6 searchImage">
                        <img
                          src={product.image}
                          className="img-fluid rounded-start searchPicture"
                          alt="product"
                        />
                      </div>

                      <div className="col-md-6">
                        <div className="card-body mainBody p-3">
                          <h5 className="card-title">{product.title}</h5>
                          <div className="searchRating">
                            {product.rating.rate}
                            <img
                              className="img-fluid"
                              src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                            />
                          </div>

                          <p className="card-text">
                            <ul>
                              {product.description.slice(0, 3).map((detail) => {
                                return <li>{detail}</li>;
                              })}
                            </ul>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              );
            })}
        {this.props.state.search &&
          products
            .filter((product) => {
              return product.description.filter((detail) => {
                return detail
                  .toLowerCase()
                  .includes(this.props.state.search.toLowerCase);
              }).length;
            })

            .map((product) => {
              return (
                <Link to={`/product:${product.id}`} className="mainSearch">
                  <div className="card  result">
                    <div className="row g-0 resultContainer">
                      <div className="col-md-6 searchImage">
                        <img
                          src={product.image}
                          className="img-fluid rounded-start searchPicture"
                          alt="product"
                        />
                      </div>

                      <div className="col-md-6">
                        <div className="card-body mainBody p-3">
                          <h5 className="card-title">{product.title}</h5>
                          <div className="searchRating">
                            {product.rating.rate}
                            <img
                              className="img-fluid"
                              src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                            />
                          </div>

                          <p className="card-text">
                            <ul>
                              {product.description.slice(0, 3).map((detail) => {
                                return <li>{detail}</li>;
                              })}
                            </ul>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              );
            })}
      </div>
    );
  }
}

export default MainSearch;
