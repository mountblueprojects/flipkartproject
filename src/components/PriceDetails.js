import React, { Component } from "react";
import "./PriceDetails.css";
import products from "../products.json";

export class PriceDetails extends Component {
  constructor(props) {
    super(props);
    this.products = products.filter((product) => {
      return Object.keys(this.props.state.cart).includes(product.id.toString());
    });
  }

  render() {
    return (
      Object.keys(this.props.state.cart).length !== 0 && (
        <div>
          <div class="card priceDetailsCard">
            <div class="card-body priceDetails">
              <h5 class="card-title">PRICE DETAILS</h5>
              <div class="card-text detailField">
                <div>Price</div>
                <div>
                  ₹
                  {this.products
                    .reduce((accumulator, current) => {
                      accumulator +=
                        parseInt(
                          current.price.substring(1).split(",").join("")
                        ) * parseInt(this.props.state.cart[current.id]);
                      return accumulator;
                    }, 0)
                    .toFixed(2)}
                </div>
              </div>
              <div class="card-text detailField">
                <div>Discount</div>
                <div className="discountPrice">
                  -₹
                  {(
                    0.05 *
                    this.products.reduce((accumulator, current) => {
                      accumulator +=
                        parseInt(
                          current.price.substring(1).split(",").join("")
                        ) * parseInt(this.props.state.cart[current.id]);
                      return accumulator;
                    }, 0)
                  ).toFixed(2)}
                </div>
              </div>

              <div class="card-text detailField">
                <div>Delivery Charges</div>
                <div className="discountPrice">FREE</div>
              </div>

              <div class="card-text detailField final">
                <div>Total Amount</div>
                <div>
                  ₹
                  {(
                    0.95 *
                    this.products.reduce((accumulator, current) => {
                      accumulator +=
                        parseInt(
                          current.price.substring(1).split(",").join("")
                        ) * parseInt(this.props.state.cart[current.id]);
                      return accumulator;
                    }, 0)
                  ).toFixed(2)}
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    );
  }
}

export default PriceDetails;
