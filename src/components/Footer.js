import React, { Component } from "react";
import "./Footer.css";

export class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <ul>
          <li>Become a Seller</li>
          <li>Advertise</li>
          <li>Gift Cards</li>
          <li>Help Center</li>
          <li>© 2007-2022 Flipkart.com</li>
        </ul>
      </div>
    );
  }
}

export default Footer;
