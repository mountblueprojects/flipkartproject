import React, { Component } from "react";
import "./Signup.css";

export class Signup extends Component {
  render() {
    return (
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content signPopup">
            <button
              id="signClose"
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
            <div className="modal-body">
              <div className="loginPicture">
                {this.props.state.isLogin && (
                  <div>
                    <div className="loginTitle">Login</div>
                    <div className="loginWelcome">
                      Get access to your Orders, Wishlist and Recommendations
                    </div>
                  </div>
                )}
                {this.props.state.isSignup && (
                  <div>
                    <div className="loginTitle">
                      Looks like you're new here!
                    </div>
                    <div className="loginWelcome">
                      Sign up with your email to get started
                    </div>
                  </div>
                )}
              </div>
              <div className="loginDetails">
                <form onSubmit={this.props.onSubmit}>
                  <div>
                    <input
                      id="emailInput"
                      placeholder="Enter Email"
                      className="input-box"
                      onChange={this.props.onChange}
                    />
                    {!this.props.state.isEmail && (
                      <p className="errorMessage">
                        Please enter valid Email ID
                      </p>
                    )}
                  </div>

                  <div>
                    <input
                      id="passwordInput"
                      type="password"
                      placeholder="Enter Password"
                      className="input-box"
                    />
                    {this.props.state.emptyPassword && (
                      <p className="errorMessage">
                        Please enter password of 8 characters
                      </p>
                    )}
                  </div>

                  {this.props.state.isSignup && (
                    <div>
                      <input
                        id="confirmPasswordInput"
                        type="password"
                        placeholder="Confirm Password"
                        className="input-box"
                      />
                      {!this.props.state.matchPasswords && (
                        <p className="errorMessage">Passwords do not match</p>
                      )}
                    </div>
                  )}

                  {this.props.state.isLogin && !this.props.state.signupSuccess && (
                    <button
                      id="confirmLogin"
                      className="loginSubmit btn btn-warning"
                      type="submit"
                      data-bs-dismiss={!this.props.state.modal && "modal"}
                      onClick={this.props.onClick}
                    >
                      Login
                    </button>
                  )}
                  {this.props.state.isLogin && this.props.state.signupSuccess && (
                    <button
                      id="successfulLogin"
                      type="button"
                      className="loginSubmit btn btn-warning"
                      data-bs-dismiss={!this.props.state.modal && "modal"}
                    >
                      Login Successful! Click here to Continue
                    </button>
                  )}

                  {this.props.state.isLogin &&
                    this.props.state.notUser &&
                    this.props.state.email &&
                    this.props.state.password && (
                      <p className="errorMessage">User Not Found</p>
                    )}
                  {this.props.state.invalidPassword && (
                    <p className="errorMessage">Invalid Password</p>
                  )}

                  {this.props.state.isSignup &&
                    !this.props.state.signupSuccess && (
                      <button
                        id="confirmSignup"
                        className="loginSubmit btn btn-warning"
                        type="submit"
                        data-bs-dismiss={!this.props.state.modal && "modal"}
                        onClick={this.props.onClick}
                      >
                        Signup
                      </button>
                    )}
                  {this.props.state.isSignup && this.props.state.signupSuccess && (
                    <button
                      id="successfulSignup"
                      type="button"
                      className="loginSubmit btn btn-warning"
                      data-bs-dismiss={!this.props.state.modal && "modal"}
                    >
                      Signup Successful! Click here to Continue
                    </button>
                  )}

                  {this.props.state.alreadyUser && (
                    <p className="errorMessage">User already exists!</p>
                  )}

                  {this.props.state.isLogin && (
                    <div>
                      <button
                        type="button"
                        id="signup"
                        className="btn"
                        onClick={this.props.onClick}
                      >
                        New to Flipkart? Create an Account
                      </button>
                    </div>
                  )}

                  {this.props.state.isSignup && (
                    <div>
                      <button
                        type="button"
                        id="login"
                        className="btn "
                        onClick={this.props.onClick}
                      >
                        Existing User? Login
                      </button>
                    </div>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Signup;
