import React, { Component } from "react";
import image from "../images/flipkart-logo.jpg";
import search from "../images/search.svg";
import "./Header.css";
import { Link } from "react-router-dom";

export class Header extends Component {
  render() {
    return (
      <nav className="mainHeader navbar sticky-top navbar-dark bg-primary">
        <div className="header container-fluid">
          <Link to="/" className="navbar-brand">
            <img id="logo" src={image} alt="logo" />
          </Link>

          <form className="d-flex" role="search">
            <div className="searchBar">
              <input
                className="searchInput form-control me-2"
                type="search"
                placeholder="Search for products, brands and more"
                aria-label="Search"
                onChange={this.props.onChange}
              />
              <Link to="/search">
                <button className="searchButton btn btn-light" type="submit">
                  <img src={search} alt="search" />
                </button>
              </Link>
            </div>

            <div id="loginSection">
              {!this.props.state.signupSuccess && !this.props.state.userLogged && (
                <button
                  id="headerLogin"
                  className="loginButton btn btn-light"
                  type="button"
                  data-bs-toggle="modal"
                  data-bs-target="#exampleModal"
                  onClick={this.props.onClick}
                >
                  Login
                </button>
              )}

              {this.props.state.signupSuccess && this.props.state.userLogged && (
                <div>
                  <button
                    className="loginButton btn btn-light"
                    type="button"
                    id="logout"
                    onClick={this.props.onClick}
                  >
                    Logout
                  </button>
                </div>
              )}
            </div>
          </form>

          <div id="navbarNav">
            <ul className="options navbar-nav">
              <li className="nav-item">
                <Link
                  to="/cart"
                  className="nav-link active"
                  aria-current="page"
                >
                  <i className="fa-solid fa-cart-shopping"></i>
                  <span>Cart</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
