import React, { Component } from "react";
import "./UpperSection.css";
import topOffers from "../images/top-offers.webp";
import appliances from "../images/appliances.webp";
import beauty from "../images/beauty.webp";
import electronics from "../images/electronics.webp";
import fashion from "../images/fashion.webp";
import grocery from "../images/grocery.webp";
import home from "../images/home.webp";
import mobiles from "../images/mobiles.webp";
import Signup from "./Signup";
import products from "../products.json";
import { Link, NavLink } from "react-router-dom";

export class UpperSection extends Component {
  render() {
    return (
      <div className="upperContainer">
        <div className="upperContent">
          <Link
            to="/"
            className="topOffers category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={topOffers} />
            <div className="categoryName">Top Offers</div>
          </Link>

          <NavLink
            to="/category/grocery"
            activeClassName="selected"
            className="grocery category card border-light"
          >
            <img
              src={grocery}
              onClick={this.props.categoryClick}
              alt="grocery"
            />
            <div className="categoryName" onClick={this.props.categoryClick}>
              Grocery
            </div>
          </NavLink>

          <NavLink
            to="/category/mobiles"
            activeClassName="selected"
            className="mobiles category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={mobiles} />
            <div className="categoryName">Mobiles</div>
          </NavLink>

          <NavLink
            to="/category/fashion"
            activeClassName="selected"
            className="fashion category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={fashion} />
            <div className="categoryName">Fashion</div>
          </NavLink>

          <NavLink
            to="/category/electronics"
            activeClassName="selected"
            className="electronics category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={electronics} />
            <div className="categoryName">Electronics</div>
          </NavLink>

          <NavLink
            to="/category/home"
            className="home category card border-light"
            activeClassName="selected"
            onClick={this.props.categoryClick}
          >
            <img src={home} />
            <div className="categoryName">Home</div>
          </NavLink>

          <NavLink
            to="/category/appliances"
            activeClassName="selected"
            className="appliances category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={appliances} />
            <div className="categoryName">Appliances</div>
          </NavLink>

          <NavLink
            to="/category/beauty"
            activeClassName="selected"
            className="beauty category card border-light"
            onClick={this.props.categoryClick}
          >
            <img src={beauty} />
            <div className="categoryName">Beauty, Toys & More</div>
          </NavLink>
        </div>

        <Signup
          state={this.props.state}
          onSubmit={this.props.onSubmit}
          onClick={this.props.onClick}
        />
      </div>
    );
  }
}

export default UpperSection;
