import React, { Component } from "react";
import "./Cards.css";
import CardSectionOne from "./CardSectionOne";
import CardSectionTwo from "./CardSectionTwo";
import CardSectionThree from "./CardSectionThree";

export class Cards extends Component {
  render() {
    return (
      <>
        <div className="cardSectionContainer">
          <CardSectionOne />
        </div>
        <div className="cardSectionContainer">
          <CardSectionTwo key="cardsTwo" />
        </div>
        <div className="cardSectionContainer">
          <CardSectionThree key="cardsThree" />
        </div>
      </>
    );
  }
}

export default Cards;
