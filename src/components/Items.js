import React, { Component } from "react";
import "./Items.css";
import products from "../products.json";
import { Link } from "react-router-dom";

export class Items extends Component {
  constructor(props) {
    super(props);
    this.products = products.filter((product) => {
      return Object.keys(this.props.state.cart).includes(product.id.toString());
    });
  }

  render() {
    return (
      <div className="productsCarts">
        {this.products.map((product) => {
          return (
            this.props.state.cart.hasOwnProperty(product.id) && (
              <div className="card mb-3 mt-3 mt-md-0 cartProduct">
                <div className="row g-0">
                  <div className="col-md-4 itemImage">
                    <img
                      src={product.image}
                      className="img-fluid rounded-start img-responsive"
                      alt="laptop"
                    />

                    <div className="cartQuantity">
                      <button
                        className={`btn cartDecrease ${product.id}`}
                        onClick={this.props.cartClick}
                      >
                        -
                      </button>
                      <div>
                        <input
                          className="form-control cartNumber"
                          type="text"
                          value={this.props.state.cart[product.id]}
                        />
                      </div>
                      <button
                        className={`btn cartIncrease ${product.id}`}
                        onClick={this.props.cartClick}
                      >
                        +
                      </button>
                    </div>
                  </div>

                  <div className="col-md-8 cartDetailsRemove px-3">
                    <div className="card-body cartDetailsRemoveCard">
                      <h5 className="card-title">{product.title}</h5>

                      {product.description.slice(0, 2).map((detail) => {
                        return <small className="text-muted">{detail}</small>;
                      })}

                      <p className="card-text singlePrice">{product.price}</p>
                    </div>
                    <button
                      className={`btn btn-light ${product.id}`}
                      onClick={this.props.cartClick}
                    >
                      REMOVE
                    </button>
                  </div>
                </div>
              </div>
            )
          );
        })}
        {Object.keys(this.props.state.cart).length > 0 && (
          <div className="checkoutButton">
            <Link to="/checkout" className="btn col-12 col-md-4 btn-warning">
              PLACE ORDER
            </Link>
          </div>
        )}
      </div>
    );
  }
}

export default Items;
