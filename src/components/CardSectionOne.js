import React, { Component } from "react";
import "./CardSection.css";
import products from "../products.json";
import { Link } from "react-router-dom";

export class CardSectionOne extends Component {
  render() {
    return (
      <div className="carouselCardsContainer">
        <div className="containerName">
          <p className="fs-2">Best of Electronics</p>
        </div>
        <div className="cardsContainer">
          <div
            id="carouselControls"
            className="carousel carousel-dark slide cardSection"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="card-group mainCards">
                  {products.slice(0, 3).map((product) => {
                    return (
                      <div className="card" key={product.id}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>
                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="carousel-item">
                <div className="card-group mainCards">
                  {products.slice(3, 6).map((product) => {
                    return (
                      <div className="card" onClick={this.props.cardClick}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>

                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="carousel-item">
                <div className="card-group mainCards">
                  {products.slice(6, 9).map((product) => {
                    return (
                      <div className="card" onClick={this.props.cardClick}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>

                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>
              <button
                className="carousel-control-prev"
                type="button"
                data-bs-target="#carouselControls"
                data-bs-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button
                className="carousel-control-next"
                type="button"
                data-bs-target="#carouselControls"
                data-bs-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardSectionOne;
