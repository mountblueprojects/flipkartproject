import React, { Component } from "react";
import UpperSection from "./UpperSection";
import MiddleSection from "./MiddleSection";

export class Article extends Component {
  render() {
    return (
      <div>
        <UpperSection
          onSubmit={this.props.onSubmit}
          state={this.props.state}
          onClick={this.props.onClick}
          categoryClick={this.props.categoryClick}
        />
        <MiddleSection />
      </div>
    );
  }
}

export default Article;
