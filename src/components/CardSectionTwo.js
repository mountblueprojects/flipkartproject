import React, { Component } from "react";
import "./CardSection.css";
import { Link } from "react-router-dom";
import products from "../products.json";

export class CardSectionTwo extends Component {
  render() {
    return (
      <div className="carouselCardsContainer">
        <div
          className="containerName"
          style={{
            backgroundImage:
              'url("https://rukminim1.flixcart.com/fk-p-flap/278/278/image/b84f1c22cce1a6a3.jpg?q=90")',
            backgroundPosition: "0px bottom",
          }}
        >
          <p className="fs-2">Best of Mobiles, Fashion and Home</p>
        </div>
        <div className="cardsContainer">
          <div
            id="carouselControlsTwo"
            className="carousel carousel-dark slide cardSection"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="card-group mainCards">
                  {products.slice(9, 12).map((product) => {
                    return (
                      <div className="card" key={product.id}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>
                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="carousel-item">
                <div className="card-group mainCards">
                  {products.slice(12, 15).map((product) => {
                    return (
                      <div className="card" onClick={this.props.cardClick}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>

                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className="carousel-item">
                <div className="card-group mainCards">
                  {products.slice(15, 18).map((product) => {
                    return (
                      <div className="card" onClick={this.props.cardClick}>
                        <Link
                          className="linkImage"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <img
                            src={product.image}
                            className="card-img-top"
                            alt="product"
                          />
                        </Link>

                        <Link
                          className="linkDetails"
                          to={{ pathname: `/product:${product.id}` }}
                        >
                          <div className="card-body mainDetails">
                            <h5 className="card-title">{product.title}</h5>
                            <p className="card-text shop">Shop Now!</p>
                            <p className="card-text">{product.price}</p>
                          </div>
                        </Link>
                      </div>
                    );
                  })}
                </div>
              </div>
              <button
                className="carousel-control-prev"
                type="button"
                data-bs-target="#carouselControlsTwo"
                data-bs-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Previous</span>
              </button>
              <button
                className="carousel-control-next"
                type="button"
                data-bs-target="#carouselControlsTwo"
                data-bs-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardSectionTwo;
