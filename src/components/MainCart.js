import React, { Component } from "react";
import Items from "./Items";
import PriceDetails from "./PriceDetails";
import "./MainCart.css";
import EmptyCart from "./EmptyCart";

export class MainCart extends Component {
  render() {
    return (
      <>
        {Object.keys(this.props.state.cart).length === 0 && (
          <EmptyCart state={this.props.state} />
        )}
        <div className="cartContainer">
          <Items state={this.props.state} cartClick={this.props.cartClick} />
          <PriceDetails state={this.props.state} />
        </div>
      </>
    );
  }
}

export default MainCart;
