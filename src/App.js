import "./App.css";
import SearchResults from "./pages/SearchResults";
import Cart from "./pages/Cart";
import Home from "./pages/Home";
import Product from "./pages/Product";
import Category from "./pages/Category";
import products from "./products.json";
import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import validator from "validator";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Checkout from "./pages/Checkout";
import toast, { Toaster } from "react-hot-toast";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEmail: true,
      emptyPassword: null,
      isLogin: true,
      isSignup: false,
      users: [],
      matchPasswords: true,
      notUser: null,
      invalidPassword: null,
      modal: false,
      alreadyUser: false,
      signupSuccess: false,
      userLogged: false,
      logoutSuccess: true,
      categoryName: null,
      cart: [],
      search: null,
      address: {},
      hasAddress: false,
      nameError: false,
      mobileError: false,
      pincodeError: false,
      localityError: false,
      addressError: false,
      cityError: false,
      stateError: false,
      name: "",
      mobile: "",
      pincode: "",
      locality: "",
      addressArea: "",
      city: "",
      state: "State",
      landmark: "",
      alternate: "",
    };
  }

  onSubmit = (event) => {
    event.preventDefault();
    if (this.state.isLogin) {
      if (event.target.children[1].children[0].value.length < 8) {
        this.setState({ emptyPassword: true });
      } else {
        this.setState({
          emptyPassword: false,
          password: event.target.children[1].children[0].value,
        });
      }

      if (validator.isEmail(event.target.children[0].children[0].value)) {
        this.setState({
          isEmail: true,
          email: event.target.children[0].children[0].value,
        });
      } else {
        this.setState({ isEmail: false });
      }

      if (this.state.isEmail && !this.state.emptyPassword) {
        if (
          this.state.users.filter((user) => {
            return user.email === event.target.children[0].children[0].value;
          }).length === 1
        ) {
          this.setState({
            signupSuccess: true,
            userLogged: true,
            notUser: false,
          });
          toast.success("Logged in Successfully");
        } else {
          this.setState({ notUser: true });
        }
      }
    } else if (this.state.isSignup) {
      if (!validator.isEmail(event.target.children[0].children[0].value)) {
        this.setState({ isEmail: false });
      } else {
        this.setState({ isEmail: true });
      }

      if (event.target.children[1].children[0].value.length < 8) {
        this.setState({ emptyPassword: true });
      } else {
        this.setState({ emptyPassword: false });
      }

      if (
        event.target.children[1].children[0].value !==
        event.target.children[2].children[0].value
      ) {
        this.setState({ matchPasswords: false });
      } else {
        this.setState({ matchPasswords: true });
      }

      if (
        this.state.users.filter((user) => {
          return user.email === event.target.children[0].children[0].value;
        }).length === 1
      ) {
        this.setState({ alreadyUser: true });
      } else if (
        event.target.children[0].children[0].value &&
        event.target.children[1].children[0].value.length >= 8 &&
        event.target.children[2].children[0].value.length >= 8 &&
        event.target.children[1].children[0].value ===
          event.target.children[2].children[0].value
      ) {
        this.setState({
          users: [
            ...this.state.users,
            {
              email: event.target.children[0].children[0].value,
              password: event.target.children[1].children[0].value,
            },
          ],
          modal: false,
          matchPasswords: true,
          emptyPassword: false,
          isEmail: true,
          signupSuccess: true,
          userLogged: true,
        });
        toast.success("Signup Successful");
      }
    }
  };

  onClick = (event) => {
    if (
      event.target.id === "headerLogin" ||
      event.target.id === "checkoutLogin"
    ) {
      this.setState({ modal: true });
    } else if (event.target.id === "login") {
      this.setState({
        isLogin: true,
        isSignup: false,
        alreadyUser: false,
        notUser: false,
      });
    } else if (event.target.id === "logout") {
      this.setState({
        userLogged: false,
        signupSuccess: false,
        logoutSuccess: true,
      });
      toast.success("Successfully Logged Out");
    } else if (event.target.id === "signup") {
      this.setState({ isLogin: false, isSignup: true });
    } else if (event.target.id === "confirmLogin") {
      if (
        this.state.users.filter((user) => {
          return (
            user.email ===
            event.target.parentElement.children[0].children[0].value
          );
        }).length === 0
      ) {
        this.setState({ notUser: true, modal: true });
      } else if (
        this.state.users.filter((user) => {
          return (
            user.password ===
            event.target.parentElement.children[1].children[0].value
          );
        }).length === 0
      ) {
        this.setState({ invalidPassword: true, modal: true });
      } else {
        this.setState({ notUser: false, invalidPassword: false, modal: false });
      }
    }
  };

  categoryClick = (event) => {
    if (event.target.parentElement.classList[0] === "topOffers") {
      this.setState({ isCategory: false });
    } else {
      this.setState({
        categoryName: event.target.classList[0],
      });
    }
  };

  addToCart = (event) => {
    const product = products.filter((product) => {
      return product.id === parseInt(event.target.classList[2]);
    })[0];

    if (this.state.cart.hasOwnProperty(product.id)) {
      this.setState({
        cart: {
          ...this.state.cart,
          [product.id]: this.state.cart[product.id] + 1,
        },
      });
    } else {
      this.setState({
        cart: {
          ...this.state.cart,
          [product.id]: 1,
        },
      });
    }
    toast.success("Product Added to Cart!", { position: "bottom-center" });
  };

  cartClick = (event) => {
    if (event.target.classList[1] === "cartDecrease") {
      if (this.state.cart[event.target.classList[2]] !== 1) {
        this.setState({
          cart: {
            ...this.state.cart,
            [event.target.classList[2]]:
              this.state.cart[event.target.classList[2]] - 1,
          },
        });
      } else {
        const newState = this.state.cart;
        delete newState[event.target.classList[2]];
        this.setState({ cart: newState });
      }
      toast.success("Product removed from Cart", { position: "bottom-center" });
    } else if (event.target.classList[1] === "cartIncrease") {
      this.setState({
        cart: {
          ...this.state.cart,
          [event.target.classList[2]]:
            this.state.cart[event.target.classList[2]] + 1,
        },
      });
      toast.success("Product added to Cart!", { position: "bottom-center" });
    } else {
      const newState = this.state.cart;
      delete newState[event.target.classList[2]];
      this.setState({ cart: newState });
      toast.success("Item removed from Cart!", { position: "bottom-center" });
    }
  };

  onChange = (event) => {
    this.setState({ search: event.target.value });
  };

  addressSubmit = (event) => {
    event.preventDefault();
    if (event.target.classList[2] === "editAddress") {
      this.setState({ hasAddress: false });
    } else if (event.target.classList[2] === "deleteAddress") {
      this.setState({
        hasAddress: false,
        address: [],
        name: "",
        mobile: "",
        pincode: "",
        locality: "",
        addressArea: "",
        city: "",
        state: "State",
        landmark: "",
        alternate: "",
      });
    } else {
      if (
        !validator.isAlpha(
          event.target.children[0].children[0].children[0].value
        )
      ) {
        this.setState({ nameError: true });
      } else {
        this.setState({ nameError: false });
      }

      if (
        !validator.isMobilePhone(
          event.target.children[0].children[1].children[0].value
        )
      ) {
        this.setState({ mobileError: true });
      } else {
        this.setState({ mobileError: false });
      }

      if (
        !validator.isNumeric(
          event.target.children[0].children[2].children[0].value
        ) ||
        event.target.children[0].children[2].children[0].value.length !== 6
      ) {
        this.setState({ pincodeError: true });
      } else {
        this.setState({ pincodeError: false });
      }

      if (
        validator.isEmpty(
          event.target.children[0].children[3].children[0].value
        )
      ) {
        this.setState({ localityError: true });
      } else {
        this.setState({ localityError: false });
      }

      if (
        validator.isEmpty(
          event.target.children[0].children[4].children[0].value
        )
      ) {
        this.setState({ addressError: true });
      } else {
        this.setState({ addressError: false });
      }

      if (
        validator.isEmpty(
          event.target.children[0].children[5].children[0].value
        )
      ) {
        this.setState({ cityError: true });
      } else {
        this.setState({ cityError: false });
      }

      if (event.target.children[0].children[6].children[0].value === "State") {
        this.setState({ stateError: true });
      } else {
        this.setState({ stateError: false });
      }

      if (
        event.target.children[0].children[6].children[0].value !== "State" &&
        !validator.isEmpty(
          event.target.children[0].children[5].children[0].value
        ) &&
        !validator.isEmpty(
          event.target.children[0].children[4].children[0].value
        ) &&
        !validator.isEmpty(
          event.target.children[0].children[3].children[0].value
        ) &&
        validator.isNumeric(
          event.target.children[0].children[2].children[0].value
        ) &&
        event.target.children[0].children[2].children[0].value.length === 6 &&
        validator.isMobilePhone(
          event.target.children[0].children[1].children[0].value
        ) &&
        validator.isAlpha(
          event.target.children[0].children[0].children[0].value
        )
      ) {
        this.setState({
          address: Array.from(event.target.children[0].children).reduce(
            (accumulator, current) => {
              if (
                current.children[0].localName === "input" ||
                current.children[0].localName === "select"
              ) {
                accumulator.push(current.children[0].value);
              }

              return accumulator;
            },
            []
          ),
          hasAddress: true,
          name: Array.from(event.target.children[0].children[0].children)[0]
            .value,
          mobile: Array.from(event.target.children[0].children[1].children)[0]
            .value,
          pincode: Array.from(event.target.children[0].children[2].children)[0]
            .value,
          locality: Array.from(event.target.children[0].children[3].children)[0]
            .value,
          addressArea: Array.from(
            event.target.children[0].children[4].children
          )[0].value,
          city: Array.from(event.target.children[0].children[5].children)[0]
            .value,
          state: Array.from(event.target.children[0].children[6].children)[0]
            .value,
          landmark: Array.from(event.target.children[0].children[7].children)[0]
            .value,
          alternate: Array.from(
            event.target.children[0].children[8].children
          )[0].value,
        });
      }
    }
  };

  render() {
    return (
      <div className="mainApp">
        <div>
          <Toaster />
        </div>

        <BrowserRouter>
          <Header
            onClick={this.onClick}
            state={this.state}
            onChange={this.onChange}
          />

          <Switch>
            <Route path="/" exact>
              <Home
                onSubmit={this.onSubmit}
                onClick={this.onClick}
                state={this.state}
                categoryClick={this.categoryClick}
                onChange={this.onChange}
              />
            </Route>

            <Route path="/cart" exact>
              <Cart
                onClick={this.onClick}
                state={this.state}
                onSubmit={this.onSubmit}
                cartClick={this.cartClick}
              />
            </Route>

            <Route path="/search" exact>
              <SearchResults
                onClick={this.onClick}
                state={this.state}
                onSubmit={this.onSubmit}
              />
            </Route>

            <Route
              path="/product:id"
              render={(props) => (
                <Product
                  {...props}
                  state={this.state}
                  addToCart={this.addToCart}
                  onClick={this.onClick}
                  onSubmit={this.onSubmit}
                />
              )}
            />

            <Route
              path="/category/:category"
              render={(props) => <Category {...props} state={this.state} />}
            />

            <Route path="/checkout" exact>
              <Checkout
                state={this.state}
                onClick={this.onClick}
                onSubmit={this.onSubmit}
                addressSubmit={this.addressSubmit}
              />
            </Route>
          </Switch>

          <Footer />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
